// // LOOPS
// // for
// // basic
// // for (let i = 0; i < 20; i++) {
// //   console.log(i);
// // }

// // continue

// function complexFucntion(i) {
//   // очень много сложного кода
// }

// // for (let i = 0; i < 20; i++) {
// //   if (i % 2 !== 0) {
// //     continue;
// //   }
// //   complexFucntion(i);
// // }

// // break
// // найти пользователя с именем Max

// // const users = [
// //   { name: "afasd" },
// //   { name: "asda" },
// //   { name: "Max", age: 13 },
// //   { name: "asd" },
// //   { name: "dasd" },
// // ];

// // for (let i = 0; i < users.length; i++) {
// //   if (users[i].name === "Max") {
// //     console.log(users[i].age);
// //     break;
// //   }
// // }

// // while (пока)

// // while(выполняется условие) {
// // ...выполняй этот код
// // }

// // let number = +prompt('Enter your integer')
// //1.2 --> 1
// // Math.round(number) -> 1 === number -> 1.2
// // parseInt(number) -> === number
// // number.isInteger()

// while(!Number.isInteger(+number)) {
//     number = prompt('Enter your integer')
//     console.log(number)
// }

// let result = number + 2;

// // do while

// let number;

// do {
//      number = +prompt('Enter interger value')
// } while(!Number.isInteger(+number))

// let result = number + 2;
// console.log('result', result)

// // for as while

// // for (let i = 0; i < 10; i++) {
// //   console.log(i);
// // }

// let i = 0;

// while (i < 10) {
//     console.log(i)
//     i++
// }

// // KEYWORDS

// // () => {}
// // function
// // let var const
// // if esle if
// // new
// // for
// // while
// // do
// // class
// // break
// // return
// // continue

// function comparison(value, maxValue) {
//   return value < maxValue ? true : false;
// }

// year % 400 && year % 4 !year % 100

// // && оператор логического умножения
// // ИЛИ оператор логисчкого сложения

// let a = 1;

// a > 10 && !isNaN(a)
// // false && true
// // 0 && 1
// // 0 * 1

// a > 10 || !isNaN(a)
// // false || true
// // 0 || 1
// // 0 + 1

// year % 4 === 0 && year % 100 !== 0 || year === 400

let num = 5;

//*
//**
//***
//****
//*****

let i = 0;
// Желаемое количество строк
var max = 5;
var space = "",
  star = "";


while (i < max) {
  space = "";
  star = "";
  for (let j = 0; j < max - i; j++) {
    space += " ";
  }
  for (let j = 0; j < 2 * i + 1; j++) {
    star += "*";
  }
  console.log(space + star);
  i++;
}

// let char = "*";
// let space = " ";
// let result1 = "";
// for (let i = 1; i <= num; i += 1) {
//   console.log(space.repeat(num - i) + char.repeat(i) + char.repeat(i - 1));
// }

// console.log(result1)


function factorial(number) {
    if (number <= 1) {
        return 1
    } else {
        console.log(number)
        return number * factorial(number - 1)
    }
}

console.log(factorial(5))