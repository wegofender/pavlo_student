// // basic example
// // function sum(a, b) {
// //   let result = a + b;

// //   return result;
// // }
// // let sumResult = 13
// // let sumResult = sum(5, 8);
// // console.log(sumResult);

// // no return

// function addListener(element, listener) {
//   // many code ...
//   // ...
//   element.addListener(listener);
// }
// // const button = document.querySelector("button");
// // function onClick(evt) {
// //   alert("button clicked", evt);
// // }
// // addListener(button, onClick);
// /////////////////////

// // BAD PRACTICE !!!
// //let x;
// function getWheatherList(url) {
//   // let result = goOnServerForData(url);
//   // 100 lines of code

//   // BAD PRACTICE !!!
//   // x = result
//   return result;
// }
// // good
// function proccessWheather(data) {
//   const processedData = data.map((item = item * 2));
//   return processedData;
// }
// const processedWheather = proccessWheather(wheatherList);

// const weatherList = getWeatherList("www.com");
// const processedWeather = proccessWeather(weatherList);

// // GOOD
// // const weatherList = getWeatherList("www.com");

// // function proccessWeather(data) {
// //   const processedData = data.map((item = item * 2));
// //   return processedData;
// // }
// // const processedWeather = proccessWeather(weatherList);

// // BAD
// // let weatherList = getWeatherList("www.com");
// // function proccessWeather() {
// //   weatherList = data.map((item = item * 2));
// // }

// // OKAY, BUT NOT OKAY
// // const processedWheather = proccessWeather(getWeatherList("www.com"));

// const getSum = function () {
//   console.log(arguments);
//   let result = 0;
//   for (let i = 0; i < arguments.length; i++) {
//     result += arguments[i];
//   }
//   return result;
// };

// getSum(6, 7, 19, 90, 23);

// особенности стрелочной функций
// отсутвует массивоподобный объект arguments
// можно не писать return при записи в одну строку БЕЗ {}
// отсутсвует свой this (запоминает this в момент объявления)

const getSum = (...numbers) => {
  console.log("arguments", numbers);
  let result = 0;
  for (let i = 0; i < numbers.length; i++) {
    result += numbers[i];
  }
  return result;
};
getSum(5, 7);

// const getResult = a => b => c => a + b + c;
function getResult(a) {
  return function (b) {
    return function (c) {
      return a + b + c;
    };
  };
}

const result = getResult(4)(5)(6); // 4 + 5 + 6 = 15
console.log(result);
// result = function (b) {
//   return function (c) {
//     return a + b + c;
//   };
// };

// const getSum = (a, b) => a + b;
