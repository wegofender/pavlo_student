// унарные и бинарные операторы 
// + -

// унарный 
+"12"
-42

// бинарный 
2 + 2
2 - (+"2")


// ternary

function getResultString(answer) {
    return answer ? "Good" : "Bad"
}

getResultString("test") // "Good"
getResultString() // "Bad"
getResultString("") // "Bad"
getResultString(false) // "Bad"


2 + 2 === 4 ? alert('this is true') : alert('this is false')
true ? a : b

// арифметические и логические операторы

// арифметические 

// + - * / %

let rest = 5 / 2 // 1 остаток то деления
let num = 4

if(num % 2) {
    alert('num не парное')
}

// логичские 
let a = 5;
let b = 7;

a && b // 7
a || b // 5

5 && 7 || 10 // 7


0 || 2 && 7 || 5 // 7
(0 || 2) && (7 || 5) // 7


const user = {
    name: "nikita",
    // data: {
    //     age: 10,
    //     city: "Kyiv"
    // }
}

console.log(user.data) // undefined
console.log(user.data.age) // Error !

console.log(user.data && user.data.age)
console.log(user.data?.age)


// сравнение

// > >= < <= == ===

5 > 7