1 Написать функцию, которая убирает все повторяющиеся ПОДРЯД символы
```javascript
    const removeChars = (string) => {
      // ...
    };
    
    console.log(removeChars("sssppplllliiiccceee")); // -> "splice"
    console.log(removeChars("xxx")); // -> ""
    console.log(removeChars("aaaabbbb")); // -> "ab"
````

2 Переиспользовать эту же функицю, но теперь для массива строк: 

````javascript
const removeDuplicates = (arr) => {
  // ...
}

console.log(removeDuplicates(['aaaabbb', 'mmaappp'])); // -> ["ab", "map"]
````

3 
Напишите функцию, которая возвращает последовательность (индекс начинается с 1) 
всех четных символов из строки. Если строка меньше двух или длиннее 100
символов, функция должна вернуть «invalid string».

```javascript
const evenStringsSequence = (string) => {
  // ...
}

evenStringsSequence("abcdefghijklm") //  --> ["b", "d", "f", "h", "j", "l"]
evenStringsSequence("a") //              --> "invalid string"
```

4 Напишите функцию, которая выводит "шахматную доску"
 ```javascript
const createChessboard = (number) => {
 // ....
}
createChessboard(8)
//   # # # #
//  # # # # 
//   # # # #
//  # # # # 
//   # # # #
//  # # # # 
//   # # # #
//  # # # # 

```