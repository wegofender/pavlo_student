// var i = 6

// for (var i = 0; i < 5; i++) {
//   setTimeout(() => {
//     // console.log(i);
//   }, 0);
// }

// simple
/*
string ->    "", "asda", ``, '', "0", "false"
number ->    1, 0, Infinity, -Infinity, NaN
boolean ->   true false
null ->      null // !! typeof (null) === "object"
undefined -> undefined
*/

// complex
/* 
object ->    {}, [], function
*/


// const user = null;
const user = {};
// console.log(b) // ReferenceError: b is not defined

user.name = "nikita";
console.log(user.age) // undefined



// Type coersion 

// to string
/*
let num = 1 
num.toString()
String(num)
*/

// to number
/*
let string = "hello" 
+string -> NaN

let strNumber = "42.5"
Number(string)
parseInt(strNumber) 42
parseFloat(strNumber) 42.5 
*/

// to boolean
/*
Boolean(1)
!!1
*/

// !!ALWAYS FALSE
/*
0
""
undefined
NaN
false
null
*/


